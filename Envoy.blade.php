@servers(['my-server' => 'ubuntu@54.169.248.69'])

@task('list', ['on' => $server])
ls -l
@endtask

@setup
    // folders on server
    $repository = 'git@gitlab.com:dangvanduc90/service-kong-user.git';
    $releases_dir = '/var/www/html/releases';
    $app_dir = '/var/www/html';
    $docker_dir = '/var/www/service-kong-user';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
    $keep_old_releases = 3;
@endsetup

@story('deploy')
    run_container
    clone_repository
    run_composer
    update_symlinks
    update_docker_files
    clean_old_releases
@endstory

@story('rollback')
    deployment_rollback
@endstory

@task('run_container', ['on' => $server])
    echo 'Running docker container'
    docker-compose -f {{ $docker_dir }}/docker-compose.yml up -d
@endtask

@task('clone_repository', ['on' => $server])
    echo 'Cloning repository'
    {{--docker-compose exec -T app bash -c "[ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}"--}}
    docker-compose -f {{ $docker_dir }}/docker-compose.yml exec -T app  bash -c "git clone --depth 1 {{ $repository }} -b {{ $branch }} {{ $new_release_dir }} \
        && chown nginx:nginx {{ $new_release_dir }} -R"
    {{--cd {{ $releases_dir }}--}}
    {{--git reset --hard {{ $commit }}--}}
@endtask

@task('run_composer', ['on' => $server])
    echo "Starting deployment ({{ $release }})"
    {{--composer install --prefer-dist --no-scripts -q -o--}}
    docker-compose -f {{ $docker_dir }}/docker-compose.yml exec -T app bash -c "cd {{ $new_release_dir }} \
        && composer install --no-dev --prefer-dist --no-scripts -o -q --optimize-autoloader\
        && chown nginx:nginx . -R"
@endtask

@task('update_symlinks', ['on' => $server])
    echo "Linking storage, .env, current release directory"
    docker-compose -f {{ $docker_dir }}/docker-compose.yml exec -T app bash -c "rm -rf {{ $new_release_dir }}/storage \
        && ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage \
        && ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env \
        && ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current"
@endtask

@task('update_docker_files', ['on' => $server])
    echo "Updating docker files"
    docker-compose -f {{ $docker_dir }}/docker-compose.yml exec -T app bash -c "cp {{ $new_release_dir }}/docker-compose.yml {{ $app_dir }}/docker-compose.yml \
        && cp {{ $new_release_dir }}/vhost.conf {{ $app_dir }}/vhost.conf"
@endtask

@task('clean_old_releases', ['on' => $server])
    echo "Clean old releases"
    cd {{ $docker_dir }}/releases
    rm -rf $(ls -t | tail -n +{{ $keep_old_releases }});
@endtask

@task('deployment_rollback', ['on' => $server])
    echo "Rolling back to previous release"
    cd {{ $docker_dir }}
    echo "#!/bin/bash
        ln -nfs \$(find /var/www/html/releases -maxdepth 1 | sort  | tail -n 2 | head -n1) {{ $app_dir }}/current
        rm -rf \$(find /var/www/html/releases -maxdepth 1 | sort  | tail -n 1 | head -n1)" > rollback.sh
    chmod +x rollback.sh
    docker-compose -f {{ $docker_dir }}/docker-compose.yml exec -T app {{ $app_dir }}/rollback.sh
@endtask
