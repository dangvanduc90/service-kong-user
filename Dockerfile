# Set the base image for subsequent instructions
FROM richarvey/nginx-php-fpm:1.5.7

# Update packages
RUN apk update

# Install PHP and composer dependencies
# RUN apk add libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev

# Clear out the local repository of retrieved package files
# RUN apk clean

# Install supervisor for laravel queue
RUN apk add supervisor

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install pdo_mysql zip bcmath

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY composer.json composer.json
COPY composer.lock composer.lock
RUN composer install --prefer-dist --no-scripts -o -q --no-autoloader

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~1.5"
