<?php

namespace App\Jobs;

use App\User;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Hash;

class DoInsertData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $amount;
    /**
     * Create a new job instance.
     * @param int $amount
     * @return void
     */
    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $insert_data = collect();

        $faker = Faker::create();

        for($i = 0; $i < $this->amount; $i++) {
            $insert_data->push([
                'name' => $faker->name,
                'email' => $faker->unique()->email,
                'password' => Hash::make($faker->password(8, 12)),
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now(),
            ]);
        }

        foreach ($insert_data->chunk(500) as $chunk)
        {
            User::insert($chunk->toArray());
        }
    }
}
