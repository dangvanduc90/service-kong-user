<?php

namespace App\Console\Commands;

use App\Jobs\DoInsertData;
use App\User;
use Faker\Provider\Address;
use Illuminate\Bus\Queueable;
use Illuminate\Console\Command;
use Faker\Factory as Faker;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\Queue;
use Illuminate\Support\Facades\Hash;

class InsertLargeData extends Command implements ShouldQueue
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:data {amount : How many data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert large data to db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amount = $this->argument('amount');

        DoInsertData::dispatch($amount);
    }
}
